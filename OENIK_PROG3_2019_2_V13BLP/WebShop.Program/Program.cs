﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebShop.Program
{
    using System;
    using System.Xml.Linq;
    using WebShop.Data;
    using WebShop.Logic;

    /// <summary>
    /// User Interface.
    /// </summary>
    public class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                MenuExecute();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Nyomjon egy gombot a kilépéshez...");
                Console.ReadKey(true);
            }
        }

        private static void Menu()
        {
            Console.WriteLine("---------------------------------------------------------------");
            Console.WriteLine("\n- 1. Entity létrehozása                                       -");
            Console.WriteLine("\n- 2. Tábla vagy Entity kiirása                                -");
            Console.WriteLine("\n- 3. Entity módosítása                                        -");
            Console.WriteLine("\n- 4. Entity törlése                                           -");
            Console.WriteLine("\n- 5. Termékek csoportjainak átlagára                          -");
            Console.WriteLine("\n- 6. Termékek megvételének száma                              -");
            Console.WriteLine("\n- 7. Rendelések listázása vásárlók szerint                    -");
            Console.WriteLine("\n- 8. Más webáruházak kinálata                                 -");
            Console.WriteLine("\n- 0. Kilépés                                                  -");
            Console.WriteLine("\n-------------------------------------------------------------");
            Console.WriteLine("A választáshoz nyomjon meg egy billentyűt!");
        }

        private static void TablakKiirasa()
        {
            Console.WriteLine("Melyik táblával szeretne dolgozni?");
            Console.WriteLine("1. Item");
            Console.WriteLine("2. Customer");
            Console.WriteLine("3. Purchase");
            Console.WriteLine("4. PurchaseDetail");
            Console.WriteLine("0. Vissza a főmenübe");
        }

        private static void MenuExecute()
        {
            ConsoleKeyInfo c;
            do
            {
                Menu();
                c = Console.ReadKey();
                Console.Clear();
                switch (c.Key)
                {
                    case ConsoleKey.D1:
                        EntityLetrehozasMenuPont();
                        break;
                    case ConsoleKey.D2:
                        OlvasasMenuPont();
                        break;
                    case ConsoleKey.D3:
                        ModositasMenuPont();
                        break;
                    case ConsoleKey.D4:
                        TorlesMenuPont();
                        break;
                    case ConsoleKey.D5:
                        ItemAvgPrice();
                        break;
                    case ConsoleKey.D6:
                        ItemCount();
                        break;
                    case ConsoleKey.D7:
                        GetOrders();
                        break;
                    case ConsoleKey.D8:
                        Java();
                        break;
                    default:
                        break;
                }
            }
            while (c.Key != ConsoleKey.D0);
            Console.WriteLine("Biztos ki akar lépni a menüből?(i/n)");
            c = Console.ReadKey();
            if (c.Key == ConsoleKey.I)
            {
                Console.Clear();
                Console.WriteLine("Nyomjon meg egy billentyűt a program bezárásához!");
                Console.ReadKey(true);
            }
            else
            {
                Console.Clear();
                MenuExecute();
            }
        }

        private static void EntityLetrehozasMenuPont()
        {
            TablakKiirasa();
            ConsoleKeyInfo c;
            do
            {
                c = Console.ReadKey(true);
            }
            while (!int.TryParse(c.KeyChar.ToString(), out int value) || value > 4);

            switch (c.Key)
            {
                case ConsoleKey.D0:
                    return;
                case ConsoleKey.D1:
                    ItemLetrehoz();
                    break;
                case ConsoleKey.D2:
                    CustomerLetrehoz();
                    break;
                case ConsoleKey.D3:
                    PurchaseLetrehoz();
                    break;
                case ConsoleKey.D4:
                    PurchaseDetailLetrehoz();
                    break;
            }

            Console.WriteLine("Létrehozás kész!");
        }

        private static void OlvasasMenuPont()
        {
            TablakKiirasa();
            ConsoleKeyInfo c1;
            ConsoleKeyInfo c2;
            do
            {
                c1 = Console.ReadKey(true);
            }
            while (!int.TryParse(c1.KeyChar.ToString(), out int value) || value > 4);

            if (c1.Key == ConsoleKey.D0)
            {
                Console.Clear();
                return;
            }

            Console.Clear();
            Console.WriteLine("1. Egy Entity kiírása");
            Console.WriteLine("2. Egész tábla kiírása");
            c2 = Console.ReadKey(true);

            switch (c1.Key)
            {
                case ConsoleKey.D0:
                    return;
                case ConsoleKey.D1:
                    ItemKiiras(c2.Key);
                    break;
                case ConsoleKey.D2:
                    CustomerKiiras(c2.Key);
                    break;
                case ConsoleKey.D3:
                    PurchaseKiiras(c2.Key);
                    break;
                case ConsoleKey.D4:
                    PurchaseDetailKiiras(c2.Key);
                    break;
            }
        }

        private static void ModositasMenuPont()
        {
            TablakKiirasa();
            ConsoleKeyInfo c;
            do
            {
                c = Console.ReadKey(true);
            }
            while (!int.TryParse(c.KeyChar.ToString(), out int value) || value > 4);

            Console.WriteLine("Melyik indexen lévő entitást szeretné módosítani?");
            int id = int.Parse(Console.ReadLine());

            switch (c.Key)
            {
                case ConsoleKey.D0:
                    return;
                case ConsoleKey.D1:
                    ItemModositas(id);
                    break;
                case ConsoleKey.D2:
                    CustomerModositas(id);
                    break;
                case ConsoleKey.D3:
                    PurchaseModositas(id);
                    break;
                case ConsoleKey.D4:
                    PurchaseDetailModositas(id);
                    break;
                default:
                    break;
            }
        }

        private static void TorlesMenuPont()
        {
            TablakKiirasa();
            ConsoleKeyInfo c;
            do
            {
                c = Console.ReadKey(true);
            }
            while (!int.TryParse(c.KeyChar.ToString(), out int value) || value > 4);

            Console.WriteLine("Melyik indexen lévő elemet szeretné törölni?");
            int id = int.Parse(Console.ReadLine());

            switch (c.Key)
            {
                case ConsoleKey.D0:
                    return;
                case ConsoleKey.D1:
                    ItemTorles(id);
                    break;
                case ConsoleKey.D2:
                    CustomerTorles(id);
                    break;
                case ConsoleKey.D3:
                    PurchaseTorles(id);
                    break;
                case ConsoleKey.D4:
                    PurchaseDetailTorles(id);
                    break;
                default:
                    break;
            }

            Console.WriteLine("Törlés kész!");
        }

        private static void ItemLetrehoz()
        {
            Item item = ItemAdatokBekerese();
            Logic<Item> logic = new Logic<Item>();
            logic.AddEntity(item);
        }

        private static void CustomerLetrehoz()
        {
            Customer customer = CustomerAdatokBekerese();
            Logic<Customer> logic = new Logic<Customer>();
            logic.AddEntity(customer);
        }

        private static void PurchaseLetrehoz()
        {
            Logic<Purchase> logic = new Logic<Purchase>();
            Purchase purchase = PurchaseAdatokBekerese();
            logic.AddEntity(purchase);
        }

        private static void PurchaseDetailLetrehoz()
        {
            Logic<PurchaseDetail> logic = new Logic<PurchaseDetail>();
            PurchaseDetail purchaseDetail = PurchaseDetailAdatokBekerese();
            logic.AddEntity(purchaseDetail);
        }

        private static void ItemKiiras(ConsoleKey key)
        {
            Logic<Item> logic = new Logic<Item>();
            if (key == ConsoleKey.D1)
            {
                Console.WriteLine("Add meg az azonosítót!");
                int id = int.Parse(Console.ReadLine());
                Item item = logic.ReadEntity(id);
                ItemAdatok(item);
            }
            else if (key == ConsoleKey.D2)
            {
                try
                {
                    var tabla = logic.ReadEntity();
                    foreach (var item in tabla)
                    {
                        ItemAdatok(item);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static void CustomerKiiras(ConsoleKey key)
        {
            Logic<Customer> logic = new Logic<Customer>();
            if (key == ConsoleKey.D1)
            {
                Console.WriteLine("Add meg az azonosítót!");
                int id = int.Parse(Console.ReadLine());
                Customer customer = logic.ReadEntity(id);
                CustomerData(customer);
            }
            else if (key == ConsoleKey.D2)
            {
                try
                {
                    var tabla = logic.ReadEntity();
                    foreach (var customer in tabla)
                    {
                        CustomerData(customer);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static void PurchaseKiiras(ConsoleKey key)
        {
            Logic<Purchase> logic = new Logic<Purchase>();
            if (key == ConsoleKey.D1)
            {
                Console.WriteLine("Add meg az azonosítót!");
                int id = int.Parse(Console.ReadLine());
                Purchase purchase = logic.ReadEntity(id);
                PurchaseData(purchase);
            }
            else if (key == ConsoleKey.D2)
            {
                try
                {
                    var tabla = logic.ReadEntity();
                    foreach (var purchase in tabla)
                    {
                        PurchaseData(purchase);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static void PurchaseDetailKiiras(ConsoleKey key)
        {
            Logic<PurchaseDetail> logic = new Logic<PurchaseDetail>();
            if (key == ConsoleKey.D1)
            {
                Console.WriteLine("Add meg az azonosítót!");
                int id = int.Parse(Console.ReadLine());
                PurchaseDetail purchaseDetail = logic.ReadEntity(id);
                PurchaseDetailData(purchaseDetail);
            }
            else if (key == ConsoleKey.D2)
            {
                try
                {
                    var tabla = logic.ReadEntity();
                    foreach (var purchaseDetail in tabla)
                    {
                        PurchaseDetailData(purchaseDetail);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static void ItemModositas(int id)
        {
            Logic<Item> logic = new Logic<Item>();
            Item item = ItemAdatokBekerese();
            item.ItemID = id;
            logic.Update(item, id);
        }

        private static void CustomerModositas(int id)
        {
            Logic<Customer> logic = new Logic<Customer>();
            Customer customer = CustomerAdatokBekerese();
            customer.CustomerID = id;
            logic.Update(customer, id);
        }

        private static void PurchaseModositas(int id)
        {
            Logic<Purchase> logic = new Logic<Purchase>();
            Purchase purchase = PurchaseAdatokBekerese();
            purchase.PurchaseID = id;
            logic.Update(purchase, id);
        }

        private static void PurchaseDetailModositas(int id)
        {
            Logic<PurchaseDetail> logic = new Logic<PurchaseDetail>();
            PurchaseDetail purchaseDetail = PurchaseDetailAdatokBekerese();
            purchaseDetail.PDetailID = id;
            logic.Update(purchaseDetail, id);
        }

        private static void ItemTorles(int id)
        {
            Logic<Item> logic = new Logic<Item>();
            logic.DeleteEntity(id);
        }

        private static void CustomerTorles(int id)
        {
            Logic<Customer> logic = new Logic<Customer>();
            logic.DeleteEntity(id);
        }

        private static void PurchaseTorles(int id)
        {
            Logic<Purchase> logic = new Logic<Purchase>();
            logic.DeleteEntity(id);
        }

        private static void PurchaseDetailTorles(int id)
        {
            Logic<PurchaseDetail> logic = new Logic<PurchaseDetail>();
            logic.DeleteEntity(id);
        }

        private static Item ItemAdatokBekerese()
        {
            Item item = new Item();
            Console.Write("Típus: ");
            item.Type = Console.ReadLine();
            Console.Write("Név: ");
            item.Name = Console.ReadLine();
            Console.Write("Ár: ");
            item.Price = int.Parse(Console.ReadLine());
            Console.Write("Raktáron van-e(true/false): ");
            item.InStorage = bool.Parse(Console.ReadLine());
            Console.Write("Szállitási díj: ");
            item.DeliveryCharge = decimal.Parse(Console.ReadLine());
            return item;
        }

        private static Customer CustomerAdatokBekerese()
        {
            Customer customer = new Customer();
            Console.Write("Kártyaszám: ");
            customer.CardNr = Console.ReadLine();
            Console.Write("Név: ");
            customer.Name = Console.ReadLine();
            Console.Write("Lakcím: ");
            customer.Address = Console.ReadLine();
            Console.Write("Telefonszám: ");
            customer.PhoneNr = int.Parse(Console.ReadLine());
            Console.Write("Szüetési dátum: ");
            customer.BirthDate = DateTime.Parse(Console.ReadLine());
            return customer;
        }

        private static Purchase PurchaseAdatokBekerese()
        {
            Purchase purchase = new Purchase();
            Console.Write("CustomerID: ");
            purchase.CustomerID = int.Parse(Console.ReadLine());
            Console.Write("Ki van már szállítva?: ");
            purchase.Delivered = bool.Parse(Console.ReadLine());
            Console.Write("Fizetési mód: ");
            purchase.PaymentMethod = Console.ReadLine();
            Console.Write("Bank: ");
            purchase.Bank = Console.ReadLine();
            Console.Write("Dátum: ");
            purchase.Date = DateTime.Parse(Console.ReadLine());
            Console.Write("Kuponkód: ");
            purchase.CouponCode = Console.ReadLine();
            return purchase;
        }

        private static PurchaseDetail PurchaseDetailAdatokBekerese()
        {
            PurchaseDetail purchaseDetail = new PurchaseDetail();
            Console.Write("PurchaseID: ");
            purchaseDetail.PurchaseID = int.Parse(Console.ReadLine());
            Console.Write("ItemID: ");
            purchaseDetail.ItemID = int.Parse(Console.ReadLine());
            Console.Write("Darabszám: ");
            purchaseDetail.Quantity = int.Parse(Console.ReadLine());
            Console.Write("Akkori egységár: ");
            purchaseDetail.PriceThen = int.Parse(Console.ReadLine());
            return purchaseDetail;
        }

        private static void ItemAdatok(Item item)
        {
            Console.WriteLine($"|Típus: {item.Type}| |Név: {item.Name}| |Ár: {item.Price}| |Raktáron van-e: {item.InStorage}| |Szállítási díj: {item.DeliveryCharge}|");
        }

        private static void CustomerData(Customer customer)
        {
            Console.WriteLine($"|Name: {customer.Name}| |Kártyaszám: {customer.CardNr}| |Lakcím: {customer.Address}| |Telefonszám: {customer.PhoneNr}| |Születési dátum: {customer.BirthDate}|");
        }

        private static void PurchaseData(Purchase purchase)
        {
            Console.WriteLine($"|Vásárló ID: {purchase.CustomerID}| |Dátum: {purchase.Date}| |Fizetési mód: {purchase.PaymentMethod}| |Bank: {purchase.Bank}| |Kuponkód: {purchase.CouponCode}| |Kiszállítva: {purchase.Delivered}|");
        }

        private static void PurchaseDetailData(PurchaseDetail purchaseDetail)
        {
            Console.WriteLine($"|Vásárlás ID: {purchaseDetail.PurchaseID}| |Termék ID: {purchaseDetail.ItemID}| |Darabszám: {purchaseDetail.Quantity}| |Akkor egységár: {purchaseDetail.PriceThen}|");
        }

        private static void ItemAvgPrice()
        {
            Logic<Item> logic = new Logic<Item>();
            var queries = logic.ItemAvgPrice();
            foreach (var item in queries)
            {
                Console.WriteLine($"|Típus: {item.Type}| |Átlagár: {item.Price}|");
            }
        }

        private static void ItemCount()
        {
            Logic<Item> logic = new Logic<Item>();
            var queries = logic.ItemCount();
            foreach (var item in queries)
            {
                Console.WriteLine($"|TermékID: {item.ItemID}| |Darab: {item.Count}|");
            }
        }

        private static void GetOrders()
        {
            Logic<Purchase> logic = new Logic<Purchase>();
            var orders = logic.GetOrders();
            foreach (var item in orders)
            {
                Console.WriteLine($"|VevőID: {item.CustomerID}| |VásárlásID: {item.PurchaseId}| |Dátum: {item.Date}| |Fizetési mód: {item.PaymentMethod}|");
            }
        }

        private static void Java()
        {
            Logic<Item> logic = new Logic<Item>();
            Console.WriteLine("Melyik indexű termék érdekel?");
            int index = int.Parse(Console.ReadLine());
            Item entity = logic.ReadEntity(index);
            decimal ar = entity.Price;
            Console.WriteLine("A kiválaszott termék neve: " + entity.Name);
            Console.WriteLine("Ára: " + ar);
            XDocument xDoc = logic.Java(ar);

            // XDocument xDoc = XDocument.Load("teszt.xml");
            Console.WriteLine("A többi webshop kinálata:");
            // var q = xDoc.Descendants("ajanlatok");
            foreach (var item in xDoc.Descendants("webshop"))
            {
                Console.WriteLine(item.Element("nev").Value);
                Console.WriteLine(item.Element("ar").Value);
            }
        }
    }
}