﻿namespace Prog4Hazi.Models
{
  public class ExerciseOutput
  {
    public string Name { get; set; }

    public double Calorie { get; set; }
  }
}