﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebShop.WebApp.Models
{
  public class CreateInput
  {
    public int ItemID { get; set; }
    public string Type { get; set; }
    public string Name { get; set; }
    public int Price { get; set; }
    public bool InStorage { get; set; }
    public int DeliveryCharge { get; set; }
  }
}