﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prog4Hazi.Models
{
  public class Exercise
  {
    public Exercise(string type, int burn)
    {
      Type = type;
      Burn = burn;
    }

    public string Type { get; set; }

    public int Burn { get; set; }
  }
}