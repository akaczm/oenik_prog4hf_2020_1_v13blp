﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prog4Hazi.Models
{
  public class ExerciseInput
  {
    public string Name { get; set; }

    public double Weight { get; set; }

    public string Type { get; set; }

    public double Minute { get; set; }
  }
}