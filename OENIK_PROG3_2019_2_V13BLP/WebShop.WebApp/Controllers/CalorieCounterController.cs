﻿using Prog4Hazi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Prog4Hazi.Controllers
{
  public class CalorieCounterController : Controller
  {
    public CalorieCounterController()
    {
      exercises = new List<Exercise>();
      exercises.Add(new Exercise("Running", 1000));
      exercises.Add(new Exercise("Yoga", 400));
      exercises.Add(new Exercise("Pilates", 472));
      exercises.Add(new Exercise("Hiking", 700));
      exercises.Add(new Exercise("Swimming", 1000));
      exercises.Add(new Exercise("Bicycle", 600));
    }

    public List<Exercise> exercises { get; set; }

    // GET: My
    public ActionResult Index()
    {
      return View();
    }

    public ActionResult Exercise()
    {
      return View("ExerciseInput");
    }

    [HttpPost]
    public ActionResult Exercise(ExerciseInput exerciseInput)
    {
      ExerciseOutput exerciseOutput = new ExerciseOutput();
      Exercise exercise = exercises.FirstOrDefault(x => x.Type == exerciseInput.Type);

      exerciseOutput.Calorie = (exerciseInput.Weight / 100) * (exerciseInput.Minute / 60) * exercise.Burn;
      exerciseOutput.Name = exerciseInput.Name;

      return View("ExerciseOutput", exerciseOutput);
    }
  }
}