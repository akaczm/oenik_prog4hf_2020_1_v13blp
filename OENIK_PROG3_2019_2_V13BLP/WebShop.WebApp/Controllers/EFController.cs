﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebShop.Data;
using WebShop.Logic;
using WebShop.Repository;
using WebShop.Repository.Interfaces;
using WebShop.WebApp.Models;

namespace WebShop.WebApp.Controllers
{
  public class EFController : Controller
  {
    public EFController()
    {
      Repository = new Repository<Item>();
      Logic = new Logic<Item>();
    }

    public IRepository<Item> Repository { get; set; }

    public ILogic<Item> Logic { get; set; }

    // GET: EF
    public ActionResult Index()
    {
      return View("Menu");
    }

    public ActionResult Click(MenuInput input)
    {
      switch (input.Option)
      {
        case "create":
          return Create();
        case "read":
          return Read();
        case "update":
          return Update();
        case "delete":
          return Delete();
        default:
          return View("Error");
      }
    }

    private ViewResult Delete()
    {
      return View("Delete");
    }

    private ViewResult Update()
    {
      return View("Update");
    }

    private ActionResult Read()
    {
      List<Item> list = Logic.ReadEntity();
      return View("Read", list);
    }

    private ActionResult Create()
    {
      return View("Create");
    }

    public ActionResult CreateEntity(Item item)
    {
      Logic.AddEntity(item);
      List<Item> list = Logic.ReadEntity();
      return View("Read", list);
    }

    public ActionResult UpdateEntity(Item item)
    {
      Logic.Update(item, item.ItemID);
      List<Item> list = Logic.ReadEntity();
      return View("Read", list);
    }

    public ActionResult DeleteEntity(int ItemID)
    {
      Logic.DeleteEntity(ItemID);
      List<Item> list = Logic.ReadEntity();
      return View("Read", list);
    }
  }
}