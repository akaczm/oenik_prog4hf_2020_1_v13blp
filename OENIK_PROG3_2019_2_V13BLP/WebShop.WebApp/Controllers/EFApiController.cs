﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Http;
using WebShop.Data;
using WebShop.Logic;
using WebShop.Repository;

namespace WebShop.WebApp.Controllers
{
  public class EFApiController : ApiController
  {
    private ILogic<Item> logic;

    public EFApiController()
    {
      logic = new Logic<Item>();
      Repository<Item> repo = new Repository<Item>();
    }

    // api/EFApi/all
    [ActionName("all")]
    [HttpGet]
    public IEnumerable<Item> GetAll()
    {
      var cars = logic.ReadEntity();
      return cars;
    }

    // api/EFApi/del/42
    [ActionName("del")]
    [HttpGet]
    public void Del(int id)
    {
      logic.DeleteEntity(id);
    }

    // api/EFApi/add
    [ActionName("add")]
    [HttpPost]
    public void Add(Item item)
    {
      logic.AddEntity(item);
    }

    // api/EFApi/upd
    [ActionName("upd")]
    [HttpPost]
    public void Upd(Item item, int id)
    {
      logic.Update(item, id);
    }
  }
}
