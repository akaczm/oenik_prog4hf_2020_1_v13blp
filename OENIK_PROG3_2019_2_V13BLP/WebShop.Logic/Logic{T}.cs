﻿// <copyright file="Logic{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebShop.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using WebShop.Data;
    using WebShop.Repository;
    using WebShop.Repository.Interfaces;

    /// <summary>
    /// Business Logic.
    /// </summary>
    /// <typeparam name="T">Type of the Entity.</typeparam>
    public class Logic<T> : ILogic<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Logic{T}"/> class.
        /// </summary>
        public Logic()
        {
            this.Repository = new Repository<T>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic{T}"/> class.
        /// </summary>
        /// <param name="repository">Repository.</param>
        public Logic(IRepository<T> repository)
        {
            this.Repository = repository;
        }

        /// <summary>
        /// Gets or sets the Repository.
        /// </summary>
        public IRepository<T> Repository { get; set; }

        /// <summary>
        /// Adds an Entity to the Database.
        /// </summary>
        /// <param name="entity">The Entity.</param>
        public void AddEntity(T entity)
        {
            this.Repository.AddEntity(entity);
        }

        /// <summary>
        /// Returns all Entities.
        /// </summary>
        /// <returns>Type of the Entity.</returns>
        public List<T> ReadEntity()
        {
            return this.Repository.ReadEntity();
        }

        ///// <summary>
        ///// Return whole table.
        ///// </summary>
        ///// <returns>IQueryable.</returns>
        // public IQueryable<T> ReadEntit()
        // {
        //     return this.repository.ReadEntit();
        // }

        /// <summary>
        /// Returns an Entity.
        /// </summary>
        /// <param name="id">Entity id.</param>
        /// <returns>Entity if exists, otherwise null.</returns>
        public T ReadEntity(int id)
        {
            return this.Repository.ReadEntity(id);
        }

        /// <summary>
        /// Updates an Entity.
        /// </summary>
        /// <param name="update">Detached Entity with new Data.</param>
        /// <param name="id">Entity id.</param>
        public void Update(T update, int id)
        {
            this.Repository.Update(update, id);
        }

        /// <summary>
        /// Deletes an Entity.
        /// </summary>
        /// <param name="id">Entity id.</param>
        public void DeleteEntity(int id)
        {
            this.Repository.DeleteEntity(id);
        }

        /// <summary>
        /// Groups items by type, and returns average price.
        /// </summary>
        /// <returns>List with Query objects.</returns>
        public List<Query> ItemAvgPrice()
        {
            return this.Repository.ItemAvgPrice();
        }

        /// <summary>
        /// Counts how many item was bought groupped by itemid.
        /// </summary>
        /// <returns>List with query objects.</returns>
        public List<Query> ItemCount()
        {
            return this.Repository.ItemCount();
        }

        /// <summary>
        /// Gets all the orders groupped by customerid.
        /// </summary>
        /// <returns>List with Query objects.</returns>
        public List<Query> GetOrders()
        {
            return this.Repository.GetOrders();
        }

        /// <summary>
        /// Returns xml from the java servlet.
        /// </summary>
        /// <param name="ar">Price of the entity.</param>
        /// <returns>Companies and their prices.</returns>
        public XDocument Java(decimal ar)
        {
            return XDocument.Load("http://localhost:8080/WebShop.Java/Ajanlatok?ar=" + ar);
        }
    }
}
