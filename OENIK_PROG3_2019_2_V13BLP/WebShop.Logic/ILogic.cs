﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebShop.Logic
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Business Logic Interface.
    /// </summary>
    /// <typeparam name="T">Type of the Entity.</typeparam>
    public interface ILogic<T>
        where T : class
    {
        /// <summary>
        /// Adds an Entity to the Database.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void AddEntity(T entity);

        /// <summary>
        /// Returns all Entities.
        /// </summary>
        /// <returns>Entity list.</returns>
        List<T> ReadEntity();

        /// <summary>
        /// Returns an Entity.
        /// </summary>
        /// <param name="id">Entity id.</param>
        /// <returns>T Entity if exists, otherwise null.</returns>
        T ReadEntity(int id);

        /// <summary>
        /// Updates an Entity in the Database.
        /// </summary>
        /// <param name="entity">Detached Entity with new Data.</param>
        /// <param name="id">Entity id.</param>
        void Update(T entity, int id);

        /// <summary>
        /// Deletes an Entity.
        /// </summary>
        /// <param name="id">Entity id.</param>
        void DeleteEntity(int id);
    }
}
