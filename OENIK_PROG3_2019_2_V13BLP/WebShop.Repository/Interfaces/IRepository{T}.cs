﻿// <copyright file="IRepository{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebShop.Repository.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Generic Repository Interface.
    /// </summary>
    /// <typeparam name="T">Type of the Repository.</typeparam>
    public interface IRepository<T> : IRepository
        where T : class
    {
        /// <summary>
        /// Adds an Entity to the Database.
        /// </summary>
        /// <typeparam name="T">Type of the Entity.</typeparam>
        /// <param name="entity">The Entity.</param>
        void AddEntity(T entity);

        /// <summary>
        /// Returns all Entities.
        /// </summary>
        /// <returns>Entity list.</returns>
        List<T> ReadEntity();

        /// <summary>
        /// Returns an Entity.
        /// </summary>
        /// <param name="id">Entity id.</param>
        /// <returns>Entity if exists, otherwise null.</returns>
        T ReadEntity(int id);

        /// <summary>
        /// Updates an Entity.
        /// </summary>
        /// <param name="update">Detached Entity with new Data.</param>
        /// <param name="id">Entity id.</param>
        void Update(T update, int id);

        /// <summary>
        /// Deletes an Entity.
        /// </summary>
        /// <typeparam name="T">Type of the Entity.</typeparam>
        /// <param name="id">Entity id.</param>
        void DeleteEntity(int id);

        /// <summary>
        /// Groups items by Type, and returns average Price.
        /// </summary>
        /// <returns>List with Query objects.</returns>
        List<Query> ItemAvgPrice();

        /// <summary>
        /// Counts how many Item was bought groupped by ItemID.
        /// </summary>
        /// <returns>List with query objects.</returns>
        List<Query> ItemCount();

        /// <summary>
        /// Gets all the orders groupped by CustomerID.
        /// </summary>
        /// <returns>List with Query objects.</returns>
        List<Query> GetOrders();
    }
}
