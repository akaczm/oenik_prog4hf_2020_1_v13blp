﻿// <copyright file="Query.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebShop.Repository
{
    using System;

    /// <summary>
    /// Used to send data to main Program.
    /// </summary>
    public class Query
    {
        /// <summary>
        /// Gets or sets Type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets Price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets ItemID.
        /// </summary>
        public int ItemID { get; set; }

        /// <summary>
        /// Gets or sets CustomerID.
        /// </summary>
        public int CustomerID { get; set; }

        /// <summary>
        /// Gets or sets the count of something.
        /// </summary>
        public decimal Count { get; set; }

        /// <summary>
        /// Gets or sets PurchaseID.
        /// </summary>
        public int PurchaseId { get; set; }

        /// <summary>
        /// Gets or sets Date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets PaymentMethod.
        /// </summary>
        public string PaymentMethod { get; set; }
    }
}
