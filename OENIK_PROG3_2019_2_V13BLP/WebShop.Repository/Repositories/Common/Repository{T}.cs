﻿// <copyright file="Repository{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using WebShop.Data;
    using WebShop.Repository.Interfaces;

    /// <summary>
    /// Generic Repository.
    /// </summary>
    /// <typeparam name="T">Type of the Repository.</typeparam>
    public class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Adds Entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public void AddEntity(T entity)
        {
            using (DbModel model = new DbModel())
            {
                model.Set<T>().Add(entity);
                model.SaveChanges();
            }
        }

        /// <summary>
        /// Returns all Entities.
        /// </summary>
        /// <returns>Entity list.</returns>
        public List<T> ReadEntity()
        {
            using (DbModel model = new DbModel())
            {
                List<T> list = new List<T>();
                DbSet<T> entities = model.Set<T>();
                foreach (var item in entities)
                {
                    list.Add(item);
                }

                return list;
            }
        }

        ///// <summary>
        ///// Returns whole table.
        ///// </summary>
        ///// <returns>IQueryable.</entities></returns>
        // public IQueryable<T> ReadEntit()
        // {
        //    using (DbModel model = new DbModel())
        //    {
        //        IQueryable<T> szar = model.Set<T>();
        //        return szar;
        //    }
        // }

        /// <summary>
        /// Returns an Entity.
        /// </summary>
        /// <param name="id">Entity id.</param>
        /// <returns>Entity if exists, otherwise null.</returns>
        public T ReadEntity(int id)
        {
            using (DbModel model = new DbModel())
            {
                return model.Set<T>().Find(id);
            }
        }

        /// <summary>
        /// Updates an Entity.
        /// </summary>
        /// <param name="update">Detached Entity with new Data.</param>
        /// <param name="id">Entity id.</param>
        public void Update(T update, int id)
        {
            using (DbModel model = new DbModel())
            {
                T modositando = model.Set<T>().Find(id);
                model.Entry(modositando).CurrentValues.SetValues(update);
                model.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes an Entity.
        /// </summary>
        /// <typeparam name="T">Type of the Entity.</typeparam>
        /// <param name="id">Entity id.</param>
        public void DeleteEntity(int id)
        {
            using (DbModel model = new DbModel())
            {
                model.Set<T>().Remove(model.Set<T>().Find(id));
                model.SaveChanges();
            }
        }

        /// <summary>
        /// Groups items by Type, and returns average Price.
        /// </summary>
        /// <returns>List with Query objects.</returns>
        public List<Query> ItemAvgPrice()
        {
            using (DbModel model = new DbModel())
            {
                var cucc = model.Set<Item>();
                return (from x in cucc
                        group x by new { x.Type } into g
                        select new Query
                        {
                            Price = g.Average(y => y.Price),
                            Type = g.Key.Type,
                        }).ToList();
            }
        }

        /// <summary>
        /// Counts how many Item was bought groupped by ItemID.
        /// </summary>
        /// <returns>List with query objects.</returns>
        public List<Query> ItemCount()
        {
            using (DbModel model = new DbModel())
            {
                return (from x in model.PurchaseDetail
                        group x by new { x.ItemID } into g
                        select new Query
                        {
                            ItemID = g.Key.ItemID,
                            Count = g.Sum(y => y.Quantity),
                        }).ToList();
            }
        }

        /// <summary>
        /// Gets all the orders groupped by CustomerID.
        /// </summary>
        /// <returns>List with Query objects.</returns>
        public List<Query> GetOrders()
        {
            using (DbModel model = new DbModel())
            {
                return (from x in model.Purchase
                        group x by new { x.CustomerID, x.PurchaseID, x.Date, x.PaymentMethod } into g
                        select new Query
                        {
                            CustomerID = g.Key.CustomerID,
                            PurchaseId = g.Key.PurchaseID,
                            Date = g.Key.Date,
                            PaymentMethod = g.Key.PaymentMethod,
                        }).ToList();
            }
        }
    }
}
