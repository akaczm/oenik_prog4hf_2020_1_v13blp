﻿// <copyright file="Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit;
    using NUnit.Framework;
    using WebShop.Data;
    using WebShop.Logic;
    using WebShop.Repository;
    using WebShop.Repository.Interfaces;

    /// <summary>
    /// Test class.
    /// </summary>
    [TestFixture]
    public class Tests
    {
        private static Mock<IRepository<Item>> itemMock;
        private static Mock<IRepository<Customer>> customerMock;
        private static Mock<IRepository<Purchase>> purchaseMock;
        private static Mock<IRepository<PurchaseDetail>> purchaseDetailMock;

        private static Logic<Item> itemLogic;
        private static Logic<Customer> customerLogic;
        private static Logic<Purchase> purchaseLogic;
        private static Logic<PurchaseDetail> purchaseDetailLogic;

        private static List<Item> items;
        private static List<Customer> customers;
        private static List<Purchase> purchases;
        private static List<PurchaseDetail> purchaseDetails;

        /// <summary>
        /// Sets up the necessary things for testing.
        /// </summary>
        [SetUp]
        public static void SetupRepositories()
        {
            itemMock = new Mock<IRepository<Item>>();
            customerMock = new Mock<IRepository<Customer>>();
            purchaseMock = new Mock<IRepository<Purchase>>();
            purchaseDetailMock = new Mock<IRepository<PurchaseDetail>>();

            itemLogic = new Logic<Item>(itemMock.Object);
            customerLogic = new Logic<Customer>(customerMock.Object);
            purchaseLogic = new Logic<Purchase>(purchaseMock.Object);
            purchaseDetailLogic = new Logic<PurchaseDetail>(purchaseDetailMock.Object);

            items = new List<Item>();
            items.Add(new Item() { ItemID = 0, Type = "CPU", Name = "Ryzen 5 2600", Price = 25000, InStorage = true, DeliveryCharge = 250 });
            items.Add(new Item() { ItemID = 1, Type = "CPU", Name = "I5 9400f", Price = 50000, InStorage = false, DeliveryCharge = 0 });
            customers = new List<Customer>();
            customers.Add(new Customer() { CustomerID = 0, Name = "Teszt Elek", CardNr = "123456789101", BirthDate = DateTime.Parse("2000.01.01"), Address = "Tesztváros Teszt utca", PhoneNr = 11111111111 });
            customers.Add(new Customer() { CustomerID = 1, Name = "John Doe", CardNr = "", BirthDate = DateTime.Parse("1999.12.10"), Address = "Mintaváros Minta utca", PhoneNr = 0 });
            purchases = new List<Purchase>();
            purchases.Add(new Purchase() { PurchaseID = 0, CustomerID = 0, Date = DateTime.Parse("2000.01.01"), PaymentMethod = "Készpénz", Bank = string.Empty, CouponCode = string.Empty, Delivered = true });
            purchases.Add(new Purchase() { PurchaseID = 1, CustomerID = 0, Date = DateTime.Parse("2002.02.02"), PaymentMethod = "Utalás", Bank = "Erste", CouponCode = "V13BLP", Delivered = true });
            purchaseDetails = new List<PurchaseDetail>();
            purchaseDetails.Add(new PurchaseDetail() { PDetailID = 0, PurchaseID = 0, ItemID = 0, PriceThen = 30000, Quantity = 5 });
            purchaseDetails.Add(new PurchaseDetail() { PDetailID = 1, PurchaseID = 0, ItemID = 1, PriceThen = 55000, Quantity = 2 });
            purchaseDetails.Add(new PurchaseDetail() { PDetailID = 2, PurchaseID = 1, ItemID = 1, PriceThen = 55000, Quantity = 8 });
        }

        /// <summary>
        /// Tests Item adding.
        /// </summary>
        [Test]
        public static void AddItem_Test()
        {
            Item item = new Item() { ItemID = 3, Type = "GPU", Name = "RTX 2080", Price = 250000, InStorage = true, DeliveryCharge = 0 };
            itemMock.CallBase = true;
            itemMock.Setup(x => x.AddEntity(It.IsAny<Item>())).Callback<Item>((y) => items.Add(y));

            Assert.That(items.Count, Is.EqualTo(2));
            itemLogic.AddEntity(item);
            Assert.That(items.Count, Is.EqualTo(3));
        }

        /// <summary>
        /// Tests Customer reading.
        /// </summary>
        [Test]
        public static void ReadCustomer_Test()
        {
            customerMock.CallBase = true;
            customerMock.Setup(x => x.ReadEntity()).Returns(customers);

            Assert.That(customerLogic.ReadEntity(), Is.EqualTo(customers));
        }

        /// <summary>
        /// Tests Purchase reading by ID.
        /// </summary>
        [Test]
        public static void ReadPurchaseByID_Test()
        {
            purchaseMock.CallBase = true;
            purchaseMock.Setup(x => x.ReadEntity(It.IsAny<int>())).Returns(purchases[0]);

            Assert.That(purchaseLogic.ReadEntity(0), Is.EqualTo(purchases[0]));
        }

        /// <summary>
        /// Tests PurchaseDetail updating.
        /// </summary>
        [Test]
        public static void UpdatePurchaseDetail_Test()
        {
            PurchaseDetail purchaseDetail = new PurchaseDetail() { PDetailID = 0, PurchaseID = 0, ItemID = 1, PriceThen = 55000, Quantity = 2 };
            purchaseDetailMock.CallBase = true;
            purchaseDetailMock.Setup(x => x.Update(purchaseDetail, 0)).Callback(() => purchaseDetails[0] = purchaseDetail);

            purchaseDetailLogic.Update(purchaseDetail, 0);
            Assert.That(purchaseDetails[0], Is.EqualTo(purchaseDetail));
        }

        /// <summary>
        /// Tests Item deleting.
        /// </summary>
        [Test]
        public static void DeleteItem_Test()
        {
            itemMock.CallBase = true;
            itemMock.Setup(x => x.ReadEntity(It.IsAny<int>())).Returns(items[0]);
            itemMock.Setup(x => x.DeleteEntity(It.IsAny<int>())).Callback(() => items.Remove(itemLogic.ReadEntity(0)));

            itemLogic.DeleteEntity(0);
            Assert.That(items[0].ItemID, Is.EqualTo(1));
        }

        /// <summary>
        /// Tests Item average Price groupped by Type.
        /// </summary>
        [Test]
        public static void ItemAveragePrice_Test()
        {
            var q = (from x in items
                    group x by new { x.Type } into g
                    select new Query
                    {
                        Price = g.Average(y => y.Price),
                        Type = g.Key.Type,
                    }).ToList();

            Assert.That(q.Count, Is.EqualTo(1));
            Assert.That(q[0].Type, Is.EqualTo("CPU"));
            Assert.That(q[0].Price, Is.EqualTo(37500));
        }

        /// <summary>
        /// Tests counting ordered Items.
        /// </summary>
        [Test]
        public static void ItemCount_Test()
        {
            var q = (from x in purchaseDetails
                    group x by new { x.ItemID } into g
                    select new Query
                    {
                        ItemID = g.Key.ItemID,
                        Count = g.Sum(y => y.Quantity),
                    }).ToList();

            Assert.That(q[0].ItemID, Is.EqualTo(0));
            Assert.That(q[0].Count, Is.EqualTo(5));
            Assert.That(q[1].ItemID, Is.EqualTo(1));
            Assert.That(q[1].Count, Is.EqualTo(10));
        }

        /// <summary>
        /// Tests reading orders.
        /// </summary>
        [Test]
        public static void GetOrders_Test()
        {
            var q = (from x in purchases
                    group x by new { x.CustomerID, x.PurchaseID, x.Date, x.PaymentMethod } into g
                    select new Query
                    {
                        CustomerID = g.Key.CustomerID,
                        PurchaseId = g.Key.PurchaseID,
                        Date = g.Key.Date,
                        PaymentMethod = g.Key.PaymentMethod,
                    }).ToList();

            Assert.That(q[0].CustomerID, Is.EqualTo(0));
            Assert.That(q[0].PurchaseId, Is.EqualTo(0));
            Assert.That(q[0].Date, Is.EqualTo(DateTime.Parse("2000.01.01")));
            Assert.That(q[0].PaymentMethod, Is.EqualTo("Készpénz"));
            Assert.That(q[1].CustomerID, Is.EqualTo(0));
            Assert.That(q[1].PurchaseId, Is.EqualTo(1));
            Assert.That(q[1].Date, Is.EqualTo(DateTime.Parse("2002.02.02")));
            Assert.That(q[1].PaymentMethod, Is.EqualTo("Utalás"));
        }
    }
}
