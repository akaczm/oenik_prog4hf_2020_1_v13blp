﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebShop.WPF
{
  class MainLogic
  {
    string url = "http://localhost:57155/api/EFApi/";
    HttpClient client = new HttpClient();

    void SendMessage(bool success)
    {
      string msg = success ? "Operation completed successfully." : "Operation failed.";
      Messenger.Default.Send(msg, "ItemResult"); 
    }

    public List<ItemViewModel> ApiGetItems()
    {
      string json = client.GetStringAsync(url + "all").Result;
      var list = JsonConvert.DeserializeObject<List<ItemViewModel>>(json);
      return list;
    }

    public void ApiDelItem(ItemViewModel item)
    {
      bool success = false;
      if (item!= null)
      {
        string json = client.GetStringAsync(url + "del/" + item.ItemID).Result;
        success = true;
      }
      SendMessage(success);
    }

    bool ApiEditItem(ItemViewModel item, bool isEditing)
    {
      if (item == null) return false;
      string myURL = isEditing ? url + "upd" : url + "add";

      Dictionary<string, string> postData = new Dictionary<string, string>();
      if (isEditing) postData.Add(nameof(ItemViewModel.ItemID), item.ItemID.ToString());
      postData.Add(nameof(ItemViewModel.Type), item.Type.ToString());
      postData.Add(nameof(ItemViewModel.Name), item.Name.ToString());
      postData.Add(nameof(ItemViewModel.Price), item.Price.ToString());
      postData.Add(nameof(ItemViewModel.InStorage), item.InStorage.ToString());
      postData.Add(nameof(ItemViewModel.DeliveryCharge), item.DeliveryCharge.ToString());

      string json = client.PostAsync(myURL, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
      return true;
    }

    public void EditItem(ItemViewModel item, Func<ItemViewModel, bool> editor)
    {
      ItemViewModel clone = new ItemViewModel();
      if (item != null) clone.CopyFrom(item);
      bool? success = editor?.Invoke(clone);
      if (success == true)
      {
        if (item != null) success = ApiEditItem(item, true);
        else success = ApiEditItem(item, false);
      }
      SendMessage(success == true);
    }
  }
}
