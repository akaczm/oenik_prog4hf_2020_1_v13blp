﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebShop.WPF
{
  class ItemViewModel : ObservableObject
  {
    private int itemID;
    private string type;
    private string name;
    private decimal price;
    private bool inStorage;
    private Nullable<decimal> deliveryCharge;

    public Nullable<decimal> DeliveryCharge
    {
      get { return deliveryCharge; }
      set { Set(ref deliveryCharge, value); }
    }


    public bool InStorage
    {
      get { return inStorage; }
      set { Set(ref inStorage, value); }
    }


    public decimal Price
    {
      get { return price; }
      set { Set(ref price, value); }
    }


    public string Type
    {
      get { return type; }
      set { Set(ref type, value); }
    }


    public string Name
    {
      get { return name; }
      set { Set(ref name, value); }
    }


    public int ItemID
    {
      get { return itemID; }
      set { Set(ref itemID, value); }
    }

    public void CopyFrom(ItemViewModel other)
    {
      if (other == null) return;
      this.ItemID = other.ItemID;
      this.Type = other.Type;
      this.Name = other.Name;
      this.Price= other.Price;
      this.InStorage= other.InStorage;
      this.DeliveryCharge= other.DeliveryCharge;
    }
  }
}
