﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WebShop.WPF
{
  class MainViewModel : ViewModelBase
  {
    private MainLogic logic;
    private ItemViewModel selectedItem;
    private ObservableCollection<ItemViewModel> allItems;

    public ObservableCollection<ItemViewModel> AllItems
    {
      get { return allItems; }
      set { Set(ref allItems, value); }
    }

    public ItemViewModel SelectedItem
    {
      get { return selectedItem; }
      set { Set(ref selectedItem, value); }
    }

    public ICommand AddCmd { get; private set; }
    public ICommand DelCmd { get; private set; }
    public ICommand UpdCmd { get; private set; }
    public ICommand ReadCmd { get; private set; }

    public Func<ItemViewModel, bool> EditorFunc { get; set; }

    public MainViewModel()
    {
      logic = new MainLogic();

      DelCmd = new RelayCommand(() => logic.ApiDelItem(selectedItem));
      AddCmd = new RelayCommand(() => logic.EditItem(null, EditorFunc));
      UpdCmd = new RelayCommand(() => logic.EditItem(selectedItem, EditorFunc));
      ReadCmd = new RelayCommand(() => AllItems = new ObservableCollection<ItemViewModel>(logic.ApiGetItems()));
    }
  }
}
