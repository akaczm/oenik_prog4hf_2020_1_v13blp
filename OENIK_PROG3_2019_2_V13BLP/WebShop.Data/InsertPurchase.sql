﻿INSERT INTO Purchase (CustomerID, Delivered, PaymentMethod, Bank, Couponcode, Date)
VALUES (1, 1, 'Készpénz', NULL, 'FKL200', CONVERT(DATETIME, '2016.02.23',102));
INSERT INTO Purchase (CustomerID, Delivered, PaymentMethod, Bank, Date)
VALUES (1, 1, 'Készpénz', NULL, CONVERT(DATETIME, '2018.06.15',102));
INSERT INTO Purchase (CustomerID, Delivered, PaymentMethod, Bank, Date)
VALUES (2, 1, 'Készpénz', NULL, CONVERT(DATETIME, '2017.12.07',102));
INSERT INTO Purchase (CustomerID, Delivered, PaymentMethod, Bank, Date)
 VALUES (2, 0, 'Utalás', 'Raiffeisen', CONVERT(DATETIME, '2019.10.22',102));
INSERT INTO Purchase (CustomerID, Delivered, PaymentMethod, Bank, Couponcode, Date)
VALUES (3, 0, 'Készpénz', NULL, 'KLH330', CONVERT(DATETIME, '2019.10.24',102));
INSERT INTO Purchase (CustomerID, Delivered, PaymentMethod, Bank, Date)
VALUES (3, 1, 'Utalás', 'Erste', CONVERT(DATETIME, '2018.03.24',102));
INSERT INTO Purchase (CustomerID, Delivered, PaymentMethod, Bank, Date)
VALUES (4, 0, 'Készpénz', NULL, CONVERT(DATETIME, '2019.10.23',102));
INSERT INTO Purchase (CustomerID, Delivered, PaymentMethod, Bank, Date)
VALUES (4, 0, 'Készpénz', NULL, CONVERT(DATETIME, '2019.10.23',102));