﻿CREATE TABLE [dbo].[Purchase] (
    [PurchaseID]    INT          NOT NULL IDENTITY,
    [CustomerID]    INT          NOT NULL,
    [Delivered]     BIT          NOT NULL,
    [PaymentMethod] VARCHAR (15) NOT NULL,
    [Bank]          VARCHAR (20) NULL,
    [Date]          DATE         NOT NULL,
    [CouponCode]    VARCHAR (10) NULL,
    PRIMARY KEY CLUSTERED ([PurchaseID] ASC)
);

