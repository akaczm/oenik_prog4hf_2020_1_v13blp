﻿CREATE TABLE [dbo].[Item]
(
	[ItemID] INT NOT NULL PRIMARY KEY,
    [Type] VARCHAR(20) NOT NULL,
    [Name] VARCHAR(20) NOT NULL, 
    [Price] NUMERIC(6) NOT NULL, 
    [InStorage] BIT NOT NULL, 
    [DeliveryCharge] NUMERIC(4) NULL
)

CREATE TABLE [dbo].[Customer]
(
	[CustomerID] INT NOT NULL PRIMARY KEY, 
    [CardNr] VARCHAR(12) NULL, 
    [Name] VARCHAR(20) NOT NULL, 
    [Address] VARCHAR(30) NOT NULL, 
    [PhoneNr] NUMERIC(11) NULL, 
    [BirthDate] DATE NOT NULL
)

CREATE TABLE [dbo].[Purchase]
(
	[PurchaseID] INT NOT NULL PRIMARY KEY, 
    [CustomerID] INT NOT NULL, 
    [Delivered] BIT NOT NULL, 
    [PaymentMethod] VARCHAR(15) NOT NULL, 
    [Bank] VARCHAR(20) NULL, 
    [Date] DATE NOT NULL, 
    [CouponCode] VARCHAR(10) NULL
)

CREATE TABLE [dbo].[PurchaseDetail]
(
	[PDetailID] INT NOT NULL PRIMARY KEY, 
    [ItemID] INT NOT NULL, 
    [Quantity] NUMERIC(3, 2) NOT NULL, 
    [PriceThen] INT NOT NULL 
)