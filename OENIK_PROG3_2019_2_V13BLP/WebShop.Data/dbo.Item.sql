﻿CREATE TABLE [dbo].[Item] (
    [ItemID]         INT          NOT NULL IDENTITY,
    [Type]           VARCHAR (20) NOT NULL,
    [Name]           VARCHAR (20) NOT NULL,
    [Price]          NUMERIC (6)  NOT NULL,
    [InStorage]      BIT          NOT NULL,
    [DeliveryCharge] NUMERIC (4)  NULL,
    PRIMARY KEY CLUSTERED ([ItemID] ASC)
);

