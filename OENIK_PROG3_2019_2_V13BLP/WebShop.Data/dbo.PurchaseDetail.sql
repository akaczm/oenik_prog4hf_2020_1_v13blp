﻿CREATE TABLE [dbo].[PurchaseDetail] (
    [PDetailID]  INT            NOT NULL IDENTITY,
	[PurchaseID] INT            NOT NULL,
    [ItemID]     INT            NOT NULL,
    [Quantity]   NUMERIC (5, 2) NOT NULL,
    [PriceThen]  INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([PDetailID] ASC)
);