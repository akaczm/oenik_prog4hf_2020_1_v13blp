﻿INSERT INTO Item (Type, Name, Price, InStorage, DeliveryCharge)
VALUES('CPU', 'I5 9400f', 48950, 1, 0);
INSERT INTO Item (Type, Name, Price, InStorage, DeliveryCharge)
VALUES('HDD', 'WD Blue 1TB', 11690, 0, 1000);
INSERT INTO Item (Type, Name, Price, InStorage, DeliveryCharge)
VALUES( 'HDD', 'Seagate 2TB', 17430, 0, 510);
INSERT INTO Item (Type, Name, Price, InStorage, DeliveryCharge)
VALUES('RAM', 'Kingston DDR4 2x8GB', 27500, 0, 345);
INSERT INTO Item (Type, Name, Price, InStorage, DeliveryCharge)
VALUES('RAM', 'Crucial 4GB', 6700, 1, 1000);
INSERT INTO Item (Type, Name, Price, InStorage, DeliveryCharge)
VALUES('MBD', 'ASUS B250', 14560, 0, 510);
INSERT INTO Item (Type, Name, Price, InStorage, DeliveryCharge)
VALUES('MBD', 'MSI Z370', 33400, 1, 345);
INSERT INTO Item (Type, Name, Price, InStorage, DeliveryCharge)
VALUES('GPU', 'Nvidia RTX 2080', 216000, 1, 0);
INSERT INTO Item (Type, Name, Price, InStorage, DeliveryCharge)
VALUES('GPU', 'Radeon 5700 XT' , 133400, 0, 0);