﻿INSERT INTO Customer (CardNr, Name, Address, PhoneNr, BirthDate)
VALUES (NULL, 'Nagy Géza', 'Győr Malom utca 5.', 06203356721, CONVERT(DATETIME, '1963.01.27',102));
INSERT INTO Customer (CardNr, Name, Address, PhoneNr, BirthDate)
VALUES (422961799408, 'Horváth Ilona', 'Kecskemét Kanyargós utca 26.', 06709542231, CONVERT(DATETIME, '1986.04.02',102));
INSERT INTO Customer (CardNr, Name, Address, PhoneNr, BirthDate)
VALUES (343950523280, 'Kuncsing Lao', 'Budapest Hagymacípősz utca 3.', 06302674588, CONVERT(DATETIME, '1988.07.30',102));
INSERT INTO Customer (CardNr, Name, Address, PhoneNr, BirthDate)
VALUES (470106258162, 'Beviz Elek', 'Székesfehérvár Harmat utca 17.', 06701157634, CONVERT(DATETIME, '1965.12.07',102));
INSERT INTO Customer (CardNr, Name, Address, PhoneNr, BirthDate)
VALUES (373795911205, 'Ki$$ Ronáldó', 'Budapest Sirály utca 6.', 06202569420, CONVERT(DATETIME, '2004.03.13',102));