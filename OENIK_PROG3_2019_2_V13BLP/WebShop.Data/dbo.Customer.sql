﻿CREATE TABLE [dbo].[Customer] (
    [CustomerID] INT          NOT NULL IDENTITY,
    [CardNr]     VARCHAR (12) NULL,
    [Name]       VARCHAR (20) NOT NULL,
    [Address]    VARCHAR (30) NOT NULL,
    [PhoneNr]    NUMERIC (11) NULL,
    [BirthDate]  DATE         NOT NULL,
    PRIMARY KEY CLUSTERED ([CustomerID] ASC)
);

