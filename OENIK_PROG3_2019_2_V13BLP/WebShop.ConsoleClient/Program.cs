﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebShop.ConsoleClient
{
  public class Item
  {
    public int ItemID { get; set; }
    public string Type { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
    public bool InStorage { get; set; }
    public Nullable<decimal> DeliveryCharge { get; set; }
    public override string ToString()
    {
      return $"ID = {ItemID}\t\tType = {Type}\t\tName = {Name}\t\tPrice = {Price}\t\tIsInStorage = {InStorage}\t\tDeliveryCharge = {DeliveryCharge}";
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("WAITING....");
      Console.ReadLine();

      string url = "http://localhost:57155/api/EFApi/";

      using (HttpClient client = new HttpClient())
      {
        string json = client.GetStringAsync(url + "all").Result;
        var list = JsonConvert.DeserializeObject<List<Item>>(json);
        foreach (var item in list)
        {
          Console.WriteLine(item);
        }
        Console.ReadLine();

        Dictionary<string, string> postData;
        string response;

        postData = new Dictionary<string, string>();
        postData.Add(nameof(Item.Type), "CPU");
        postData.Add(nameof(Item.Name), "Intel G4560");
        postData.Add(nameof(Item.Price), "16745");

        response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
        json = client.GetStringAsync(url + "all").Result;
        Console.WriteLine("ADD: " + response);
        Console.WriteLine("ALL: " + json);
        Console.ReadLine();

        int itemID = JsonConvert.DeserializeObject<List<Item>>(json).FirstOrDefault(x => x.Name == "Intel G4560").ItemID;
        postData = new Dictionary<string, string>();
        postData.Add(nameof(Item.ItemID), itemID.ToString());
        postData.Add(nameof(Item.Name), "Intel G4560");
        postData.Add(nameof(Item.Price), "17000");

        response = client.PostAsync(url + "upd", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
        json = client.GetStringAsync(url + "all").Result;
        Console.WriteLine("UPD: " + response);
        Console.WriteLine("ALL: " + json);
        Console.ReadLine();

        response = client.GetStringAsync(url + "del/" + itemID).Result;
        json = client.GetStringAsync(url + "all").Result;
        Console.WriteLine("DEL: " + response);
        Console.WriteLine("ALL: " + json);
        Console.WriteLine("Press a button to exit...");
        Console.ReadLine();
      }
    }
  }
}
