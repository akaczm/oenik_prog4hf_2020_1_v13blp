var searchData=
[
  ['data_45',['Data',['../namespace_web_shop_1_1_data.html',1,'WebShop']]],
  ['interfaces_46',['Interfaces',['../namespace_web_shop_1_1_repository_1_1_interfaces.html',1,'WebShop::Repository']]],
  ['logic_47',['Logic',['../namespace_web_shop_1_1_logic.html',1,'WebShop']]],
  ['program_48',['Program',['../namespace_web_shop_1_1_program.html',1,'WebShop']]],
  ['repository_49',['Repository',['../namespace_web_shop_1_1_repository.html',1,'WebShop']]],
  ['tests_50',['Tests',['../namespace_web_shop_1_1_logic_1_1_tests.html',1,'WebShop::Logic']]],
  ['webshop_51',['WebShop',['../namespace_web_shop.html',1,'']]]
];
