var searchData=
[
  ['logic_22',['Logic',['../class_web_shop_1_1_logic_1_1_logic.html',1,'WebShop.Logic.Logic&lt; T &gt;'],['../class_web_shop_1_1_logic_1_1_logic.html#a78c87092d392179efb2548ac22d0dba0',1,'WebShop.Logic.Logic.Logic()'],['../class_web_shop_1_1_logic_1_1_logic.html#af1fbfa821756a2f273fe50c2ee0e5425',1,'WebShop.Logic.Logic.Logic(IRepository&lt; T &gt; repository)']]],
  ['logic_3c_20webshop_3a_3adata_3a_3acustomer_20_3e_23',['Logic&lt; WebShop::Data::Customer &gt;',['../class_web_shop_1_1_logic_1_1_logic.html',1,'WebShop::Logic']]],
  ['logic_3c_20webshop_3a_3adata_3a_3aitem_20_3e_24',['Logic&lt; WebShop::Data::Item &gt;',['../class_web_shop_1_1_logic_1_1_logic.html',1,'WebShop::Logic']]],
  ['logic_3c_20webshop_3a_3adata_3a_3apurchase_20_3e_25',['Logic&lt; WebShop::Data::Purchase &gt;',['../class_web_shop_1_1_logic_1_1_logic.html',1,'WebShop::Logic']]],
  ['logic_3c_20webshop_3a_3adata_3a_3apurchasedetail_20_3e_26',['Logic&lt; WebShop::Data::PurchaseDetail &gt;',['../class_web_shop_1_1_logic_1_1_logic.html',1,'WebShop::Logic']]]
];
