var searchData=
[
  ['ilogic_12',['ILogic',['../interface_web_shop_1_1_logic_1_1_i_logic.html',1,'WebShop::Logic']]],
  ['irepository_13',['IRepository',['../interface_web_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'WebShop.Repository.Interfaces.IRepository'],['../interface_web_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'WebShop.Repository.Interfaces.IRepository&lt; T &gt;']]],
  ['irepository_3c_20t_20_3e_14',['IRepository&lt; T &gt;',['../interface_web_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'WebShop::Repository::Interfaces']]],
  ['item_15',['Item',['../class_web_shop_1_1_data_1_1_item.html',1,'WebShop::Data']]],
  ['itemaverageprice_5ftest_16',['ItemAveragePrice_Test',['../class_web_shop_1_1_logic_1_1_tests_1_1_tests.html#a1c5387ac34ebeb148a6c89b8b2c118ae',1,'WebShop::Logic::Tests::Tests']]],
  ['itemavgprice_17',['ItemAvgPrice',['../class_web_shop_1_1_logic_1_1_logic.html#a156f37955ebd438036311368302b62a0',1,'WebShop.Logic.Logic.ItemAvgPrice()'],['../interface_web_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html#a3c5227914c4991e26ba40ef6273b9bee',1,'WebShop.Repository.Interfaces.IRepository.ItemAvgPrice()'],['../class_web_shop_1_1_repository_1_1_repository.html#a742402691b14fb0f12effba67475427b',1,'WebShop.Repository.Repository.ItemAvgPrice()']]],
  ['itemcount_18',['ItemCount',['../class_web_shop_1_1_logic_1_1_logic.html#a95a7e32c2f7ba8463d7ca96a5e36ab7b',1,'WebShop.Logic.Logic.ItemCount()'],['../interface_web_shop_1_1_repository_1_1_interfaces_1_1_i_repository.html#aa8631ad64996adca24403ae2e98ceb2a',1,'WebShop.Repository.Interfaces.IRepository.ItemCount()'],['../class_web_shop_1_1_repository_1_1_repository.html#a5b66971c29b261893bdde20d4f106c58',1,'WebShop.Repository.Repository.ItemCount()']]],
  ['itemcount_5ftest_19',['ItemCount_Test',['../class_web_shop_1_1_logic_1_1_tests_1_1_tests.html#a9f4e9d5526e1d0606106d987bbb4b6f5',1,'WebShop::Logic::Tests::Tests']]],
  ['itemid_20',['ItemID',['../class_web_shop_1_1_repository_1_1_query.html#a04e3a14312aef97fef53d004baba062a',1,'WebShop::Repository::Query']]]
];
